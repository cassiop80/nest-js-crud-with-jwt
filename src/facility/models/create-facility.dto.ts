export class CreateFacilityDto {
  readonly name: string;
  readonly phone: string;
  readonly description: string;
}
